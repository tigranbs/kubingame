# Kubingame
This project aims to build easy to use CLI for automatically making and deploying Kubernetes Clusters on Google Cloud Platform.<br/>
It mainly using Google Node.js SDK for authentication and Google Container Engine API calls.

# Installation
This repository is basic Node.js package and as usual you need just to pull it and install NPM packages to get started.
```bash
~# git clone https://gitlab.com/tigranbs/kubingame
~# cd kubingame
~# npm install
```

You also need to download API Key JSON file from [Google Cloud Console Service Credentials](https://console.cloud.google.com/apis/credentials).
This application CLI built on top of `Google JWT Auth`, but it is possible to have other authentication processes also.

# Usage
Main CLI interface contains `--help` information on every command, so that it would be easy to see usage of every specific command.
```bash
~# ./kubingame.js --help
Commands:
  list    List available Kubernetes clusters
  create  Create new Kubernetes cluster
  delete  Delete Kubernetes Clusters
```

#### `list` - List available Kubernetes clusters
**Arguments**<br/>
`--key` or `-k` - Path to a Google JSON key file for authentication<br/>
`--zone` or `z` - Name of the Google Cloud Zone, for making specific API calls<br/>
`--json` or `-j` - Show output in JSON format<br/>

#### `create` - Create new Kubernetes cluster
**Arguments**<br/>
`--key` or `-k` - Path to a Google JSON key file for authentication<br/>
`--zone` or `z` - Name of the Google Cloud Zone, for making specific API calls<br/>
`--json` or `-j` - Show output in JSON format<br/>
`--name` or `-n` - Name for cluster<br/>
`--node-type` - VM type from Google Cloud VM types<br/>
`--node-disk-size` - Disk Size for virtual machine in GB<br/>
`--node-count` - How many nodes need to create. This could be changed later.<br/>

#### `delete` - Delete Kubernetes cluster
**Arguments**<br/>
`--key` or `-k` - Path to a Google JSON key file for authentication<br/>
`--zone` or `z` - Name of the Google Cloud Zone, for making specific API calls<br/>
`--json` or `-j` - Show output in JSON format<br/>
`--name` or `-n` - List of Names to delete, it is possible to delete multiple clusters<br/>
