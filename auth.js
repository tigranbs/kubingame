/**
 * Created by tigran on 5/22/17.
 */

const google = require('googleapis');

module.exports = (key_file) => {
    return new Promise(function (resolve, reject) {
        let key = {};
        try {
            key = require(key_file);
        } catch (e) {
            reject(e);
            return;
        }

        let jwtClient = new google.auth.JWT(
            key.client_email,
            null,
            key.private_key,
            ['https://www.googleapis.com/auth/cloud-platform'],
            null
        );

        jwtClient.authorize(function (err, tokens) {
            if (err) {
                reject(err);
                return;
            }

            resolve({ client: jwtClient, projectId: key.project_id });
        });
    });
};