#!/usr/bin/env node
/**
 * Created by tigran on 5/22/17.
 */


const yargs = require('yargs')
    , auth = require('./auth')
    // TODO: need to make some argument to handle multiple cloud providers!!
    , gcActions = require('./providers/google_cloud');

let general_args = {
    key: {
        describe: 'Google API JSON key file for authentication',
        alias: 'k',
        default: 'gc-key.json'
    },
    zone: {
        describe: 'Google Cloud Zone name for making API requests',
        alias: 'z',
        default: 'us-central1-a'
    },
    json: {
        describe: 'Get API output with in JSON format',
        boolean: true
    }
};

const argv = yargs
    .command('list', 'List available Kubernetes clusters', general_args, function (argv) {
        auth(argv.key)
            .then(auth_data => gcActions.listClusters(auth_data, argv.zone))
            .then(response => {
                if(argv.json === true) {
                    console.log(JSON.stringify(response, null, 2));
                    return;
                }

                if('clusters' in response && 'length' in response.clusters) {
                    response.clusters.map(cluster => console.log("Name: ", cluster.name, "VM Type: ", cluster.nodeConfig.machineType, "VM Disk Size GB: ", cluster.nodeConfig.diskSizeGb));
                    return;
                }

                console.log("There is no Clusters available yet!");
            })
            .catch(error => console.log(error))
    })
    .command('create', 'Create new Kubernetes cluster', Object.assign({}, general_args, {
            name: {
                alias: 'n',
                required: true,
                describe: 'Name for the cluster'
            },
            nodeType: {
                describe: 'VM type from Google Cloud VM types',
                default: 'n1-standard-1'
            },
            nodeDiskSize: {
                describe: 'Disk Size for virtual machine in GB',
                default: '10'
            },
            nodeCount: {
                describe: 'How many nodes need to create. This could be changed later.',
                default: '1'
            }
        }) , function (argv) {

        auth(argv.key)
            .then(auth_data => gcActions.createCluster(auth_data, argv.zone, argv.name, {
                machineType: argv.nodeType,
                diskSizeGb: argv.nodeDiskSize
            }, argv.nodeCount))
            .then(response => {
                if(argv.json === true) {
                    console.log(JSON.stringify(response, null, 2));
                    return;
                }

                if('name' in response && 'status' in response) {
                    console.log("Operation: ", response.name, "Status: ", response.status);
                    return;
                }

                console.log("Received response seems wrong formatted!");
                console.log(JSON.stringify(response, null, 2));
            })
            .catch(error => console.log(error))

    })
    .command('delete', 'Delete Kubernetes Clusters', Object.assign({}, general_args, {
        name: {
            alias: 'n',
            array: true,
            required: true,
            describe: 'List of cluster names to delete'
        }
    }), function (argv) {
        let pr = auth(argv.key)
            .then(auth_data => {
                return Promise.all(argv.name.map(name => {
                    return gcActions.deleteCluster(auth_data, argv.zone, name);
                }));
            })
            .catch(error => console.log(error));
    })
    .help().argv;