/**
 * Created by tigran on 5/22/17.
 */
const google = require('googleapis')
    , container = google.container('v1');

const listClusters = (auth_data, zone) => {
    return new Promise((resolve, reject) => {
        container.projects.zones.clusters.list({
            projectId: auth_data.projectId,
            zone: zone,
            auth: auth_data.client
        }, (err, response) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(response);
        });
    });
};

const createCluster = (auth_data, zone, name, nodeConfig, nodeCount = 1) => {
    return new Promise((resolve, reject) => {
        container.projects.zones.clusters.create({
            projectId: auth_data.projectId,
            zone: zone,
            auth: auth_data.client,
            resource: {
                cluster: {
                    name: name,
                    initialNodeCount: nodeCount,
                    nodeConfig: nodeConfig
                }
            }
        }, (err, response) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(response);
        });
    });
};

const deleteCluster = (auth_data, zone, name) => {
    return new Promise((resolve, reject) => {
        container.projects.zones.clusters.delete({
            projectId: auth_data.projectId,
            zone: zone,
            auth: auth_data.client,
            clusterId: name
        }, (err, response) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(response);
        });
    });
};

module.exports = {
    listClusters: listClusters,
    createCluster: createCluster,
    deleteCluster: deleteCluster,
};